//
//  MarvelAPIManager.swift
//  marvelCharacters
//
//  Created by Pablo Sierra on 13/5/17.
//  Copyright © 2017 Pablo Sierra. All rights reserved.
//

import Foundation

protocol MarvelAPIManagerDelegate: class {
    func marvelAPIManager(_ sender: MarvelAPIManager, didFetchCharacters characters:[Character])
    func marvelAPIManager(_ sender: MarvelAPIManager, didFailDownloadingCharacters errorDescription: String)
}

class MarvelAPIManager {
    static let shared = MarvelAPIManager()
    weak var delegate: MarvelAPIManagerDelegate?
    static let kLastDateUpdated = "lastDateUpdated"
    static var lastDateUpdated: Date? {
        get {
            return UserDefaults.standard.object(forKey: kLastDateUpdated) as? Date
        }
    }
    
    func getCharacters(offset: Int = 0, previousCharacters: [Character] = []) {
        DispatchQueue.global().async {
            var currentOffset = 0
            var characters: [Character] = previousCharacters
            if let request = NetworkManager.shared.createRequest(urlString: MarvelAPIConfiguration.characters(offset: offset, lastDateUpdated: MarvelAPIManager.lastDateUpdated), type: .GET, parameters: nil) {
                NetworkManager.shared.performRequest(request: request, success: { (json) in
                    if let jsonDict = json as? [String: Any], let data = jsonDict["data"] as? [String: Any], let results = data["results"] as? [[String: Any]] {
                        if results.count == 0 {
                            self.delegate?.marvelAPIManager(self, didFetchCharacters: Character.getCachedCharacters())
                        }
                        else {
                            for result in results {
                                let offlineCharacter = Character.findCreate(fromdict: result)
                                characters.append(offlineCharacter)
                            }
                            if let total = data["total"] as? Int, let offset = data["offset"] as? Int, let count = data["count"] as? Int {
                                currentOffset = offset + count
                                print("Amount of characters: \(characters.count), total: \(total)")
                                self.delegate?.marvelAPIManager(self, didFetchCharacters: characters)
                                if characters.count != total {
                                    self.getCharacters(offset: currentOffset, previousCharacters: characters)
                                }
                                else {
                                    UserDefaults.standard.set(Date(), forKey:MarvelAPIManager.kLastDateUpdated)
                                    UserDefaults.standard.synchronize()
                                }
                            }
                        }
                    }
                    else {
                        self.delegate?.marvelAPIManager(self, didFailDownloadingCharacters: "Error reading the results")
                        self.delegate?.marvelAPIManager(self, didFetchCharacters: Character.getCachedCharacters())
                    }
                }) { (failureType, code, errorDescription) in
                    self.delegate?.marvelAPIManager(self, didFailDownloadingCharacters: "Request failed with description: \(errorDescription ?? "")")
                    self.delegate?.marvelAPIManager(self, didFetchCharacters: Character.getCachedCharacters())
                }
            }
            else {
                self.delegate?.marvelAPIManager(self, didFailDownloadingCharacters: "Error creating request")
            }
        }
    }
}
