//
//  MarvelAPIConfiguration.swift
//  marvelCharacters
//
//  Created by Pablo Sierra on 13/5/17.
//  Copyright © 2017 Pablo Sierra. All rights reserved.
//

import Foundation

struct MarvelAPIConfiguration {
    
    //MARK: - Configuration
    static let kAPIKey = "" // TODO: Set Marvel API key
    static let kPrivateKey = "" // TODO: Set Marvel private key
    static let kBaseURL = "https://gateway.marvel.com/"
    static let timestamp = Date().timeIntervalSince1970.description
    static let hash = CryptoHelper.MD5(string: "\(timestamp)\(kPrivateKey)\(kAPIKey)")
    static var authParameters: String {
        get {
            return "?ts=\(MarvelAPIConfiguration.timestamp)&apikey=\(MarvelAPIConfiguration.kAPIKey)&hash=\(MarvelAPIConfiguration.hash)"
        }
    }
    static let charactersLimit = 100
    
    //MARK: - EndPoints
    static let kCharactersEndPoint = "v1/public/characters"
    
    //MARK: - Accessor methods
    static func characters(offset: Int, lastDateUpdated: Date?) -> String {
        if let lastDateUpdated = lastDateUpdated  {
            return kBaseURL + kCharactersEndPoint + authParameters + "&limit=\(charactersLimit)&offset=\(offset)&modifiedSince=\(lastDateUpdated.marvelFormat())"
        }
        else {
            return kBaseURL + kCharactersEndPoint + authParameters + "&limit=\(charactersLimit)&offset=\(offset)"
        }
        
    }
}
