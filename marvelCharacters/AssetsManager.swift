//
//  AssetsManager.swift
//  marvelCharacters
//
//  Created by Pablo Sierra on 15/5/17.
//  Copyright © 2017 Pablo Sierra. All rights reserved.
//

import Foundation
import UIKit
import CoreData

typealias fetchImageSuccess = (_ image: UIImage) -> Void
typealias fetchImageFailure = (_ errorDescription: String) -> Void

class AssetsManager {
    static let shared = AssetsManager()
    let assetsFolder = "Assets"
    
    var cache = NSCache<NSString, AnyObject>()
    
    //TODO: Refactor this to fetch all type of images.
    func fetchThumbnailImage(withCharacter character: Character, success: fetchImageSuccess, failure: fetchImageFailure) {
        let identifier = String(character.id)
        
        //Check in memory cache.
        if let image = cache.object(forKey: identifier as NSString) as? UIImage {
            success(image)
        }
        else {
            //Check in the app directory
            if let data = fileInCacheDirectory(identifier) {
                if let image = UIImage(data: data) {
                    success(image)
                }
                else {
                    failure("Error creating image")
                }
            }
            else {
                //Pull the image and save
                if let thumbnailURL = character.thumbnailURL, let data = try? Data(contentsOf: thumbnailURL) {
                    self.saveImageToDisk(data, identifier: identifier)
                    if let image = UIImage(data: data) {
                        success(image)
                    }
                    else {
                        failure("Error creating image")
                    }
                }
                else {
                    failure("Error fetching the image")
                }
            }
        }
    }
    
    //MARK: LOCAL STORAGE
    
    func getCacheDirectory() -> URL? {
        if let documentsURL = FileManager.default.urls(for: .cachesDirectory, in: .userDomainMask).first{
            return documentsURL
        }
        else {
            return nil
        }
    }
    
    func getAssetsDirectory() -> URL? {
        if let cacheDirectoryURL = getCacheDirectory() {
            return cacheDirectoryURL.appendingPathComponent(assetsFolder)
        }
        else {
            return nil
        }
    }
    
    func fileInCacheDirectory(_ identifier: String) -> Data? {
        if let assetsURL = getAssetsDirectory() {
            let fileURLPath = assetsURL.appendingPathComponent(identifier).path
            return (try? Data(contentsOf: URL(fileURLWithPath: fileURLPath)))
        }
        else {
            return nil
        }
    }
    
    func saveImageToDisk(_ dataImage: Data, identifier: String) {
        do {
            //Create the directory if it doesn't exist
            if let assetsURL = getAssetsDirectory() {
                try FileManager.default.createDirectory(at: assetsURL, withIntermediateDirectories: true, attributes: nil)
                
                //Time to save the image
                do {
                    try dataImage.write(to: assetsURL.appendingPathComponent(identifier), options: NSData.WritingOptions.atomicWrite)
                    
                } catch let errorSave as NSError {
                    print(errorSave.description)
                }
            }
            else {
                print("No assets url")
            }
            
        } catch let error as NSError {
            print(error.description)
        }
    }
}
