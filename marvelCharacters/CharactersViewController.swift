//
//  CharactersViewController.swift
//  marvelCharacters
//
//  Created by Pablo Sierra on 13/5/17.
//  Copyright © 2017 Pablo Sierra. All rights reserved.
//

import UIKit

class CharactersViewController: UIViewController, MarvelAPIManagerDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    @IBOutlet weak var charactersCollectionView: UICollectionView!
    var characters: [Character] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        MarvelAPIManager.shared.delegate = self
        MarvelAPIManager.shared.getCharacters()
        charactersCollectionView.delegate = self
    }
    
    func processAddedCharacters(_ newCharacters: [Character]) {
        charactersCollectionView.performBatchUpdates({
            let currentAmountOfCharacters = self.characters.count
            for index in currentAmountOfCharacters ..< newCharacters.count  {
                self.characters.append(newCharacters[index])
                let indexPath = IndexPath(row:index, section: 0)
                self.charactersCollectionView.insertItems(at: [indexPath])
            }
        }, completion: nil)
    }
    
    //MARK: - Marvel API manager delegate
    func marvelAPIManager(_ sender: MarvelAPIManager, didFetchCharacters characters:[Character]) {
        DispatchQueue.main.async(execute: {
            self.processAddedCharacters(characters)
        })
        
    }
    func marvelAPIManager(_ sender: MarvelAPIManager, didFailDownloadingCharacters errorDescription: String) {
        print(errorDescription)
    }
    
    //MARK: - Collection view data source
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return characters.count
    }
    
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "characterCell", for: indexPath) as? CharacterCollectionViewCell
        cell?.cellTag = indexPath.row
        
        let character = characters[indexPath.row]
        cell?.loadCharacter(character: character)
        
        DispatchQueue.global().async {
            AssetsManager.shared.fetchThumbnailImage(withCharacter: character, success: { (thumbnailImage) in
                DispatchQueue.main.async(execute: {
                    if (cell?.cellTag == indexPath.row) {
                        cell?.thumbnail.image = thumbnailImage
                        UIView.animate(withDuration: 0.2, animations: {
                            cell?.thumbnail.alpha = 1
                        }, completion: nil)
                    }
                })
            }, failure: { (errorDescription) in
                print("error: \(errorDescription)")
            })
        }
        
        return cell!
    }
    
    //MARK: - UICollectionViewDelegateFlowLayout
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let character = characters[indexPath.row]
        //TODO: Update this to make use of autolayout to calculate the size.
        if let bio = character.bio, bio.characters.count > 0 {
            return CGSize(width: self.view.frame.width, height: 530)
        }
        else {
            return CGSize(width: self.view.frame.width, height: 360 );
        }
    }
}
