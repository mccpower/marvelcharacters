//
//  CoreDataManager.swift
//  marvelCharacters
//
//  Created by Pablo Sierra on 14/5/17.
//  Copyright © 2017 Pablo Sierra. All rights reserved.
//

import Foundation
import CoreData

typealias coreDataSuccess = () -> Void
typealias coreDataFailure = (_ errorDescription: String) -> Void
typealias saveEntitySuccess = () -> Void
typealias saveEntityFailure = (_ errorDescription: String) -> Void
typealias deleteSuccess = () -> Void
typealias deleteFailure = (_ errorDescription: String) -> Void


class CoreDataManager {
    static let shared = CoreDataManager()
    static let modelName = "Marvel"
    static let kPersistentStoreCoordinatorErrorKey = "persitentStoreCoordinatorErrorKey"
    
    // MARK: - Core Data stack
    lazy var applicationDocumentsDirectory: URL = {
        let urls = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return urls[urls.count-1]
    }()
    
    lazy var managedObjectModel: NSManagedObjectModel = {
        let modelURL = Bundle.main.url(forResource: modelName, withExtension: "momd")!
        return NSManagedObjectModel(contentsOf: modelURL)!
    }()
    
    lazy var persistentStoreCoordinator: NSPersistentStoreCoordinator = {
        let coordinator = NSPersistentStoreCoordinator(managedObjectModel: self.managedObjectModel)
        let url = self.applicationDocumentsDirectory.appendingPathComponent("Marvel.sqlite")
        do {
            try coordinator.addPersistentStore(ofType: NSSQLiteStoreType, configurationName: nil, at: url, options: [NSMigratePersistentStoresAutomaticallyOption: true])
        } catch {
            NSLog("There was an error creating or loading the application's saved data.")
            abort()
        }
        
        return coordinator
    }()
    
    lazy var managedObjectContext: NSManagedObjectContext = {
        let coordinator = self.persistentStoreCoordinator
        var managedObjectContext = NSManagedObjectContext(concurrencyType: .mainQueueConcurrencyType)
        managedObjectContext.persistentStoreCoordinator = coordinator
        return managedObjectContext
    }()
    
    // MARK: - Core Data Saving support
    
    func saveContext (success: coreDataSuccess, failure: coreDataFailure) {
        if managedObjectContext.hasChanges {
            do {
                try managedObjectContext.save()
                success()
            } catch {
                let nserror = error as NSError
                failure("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
}
//Custom methods
extension CoreDataManager {
    func saveEntityWith(entityName: String, attributes: Dictionary<String, Any>, success: saveEntitySuccess, failure: saveEntityFailure) {
        let entity = NSEntityDescription.insertNewObject(forEntityName: entityName, into: self.managedObjectContext)
        for (key, value) in attributes {
            entity.setValue(value, forKey: key)
        }
        self.saveContext(success: {
            success()
        }) { (errorDescription) in
            failure(errorDescription)
        }
    }
    
    func deleteObjects(objectsToDelete: [NSManagedObject], success: deleteSuccess, failure: deleteFailure) {
        for objectToDelete in objectsToDelete {
            self.managedObjectContext.delete(objectToDelete)
        }
        self.saveContext(success: {
            success()
        }) { (errorDescription) in
            failure(errorDescription)
        }
    }
    
    func deleteObject(object: NSManagedObject, success: deleteSuccess, failure: deleteFailure) {
        deleteObjects(objectsToDelete: [object], success: {
            success()
        }) { (errorDescription) in
            failure(errorDescription)
        }
    }
    
    func allRecords<T: NSManagedObject>(type : String, sort: NSSortDescriptor? = nil) -> [T] {
        let context = CoreDataManager.shared.managedObjectContext
        var request: NSFetchRequest<NSFetchRequestResult>?
        if #available(iOS 10.0, *) {
            request = T.fetchRequest()
        } else {
            request = NSFetchRequest(entityName: type)
        }
        
        if let request = request {
            do {
                let results = try context.fetch(request)
                return results as! [T]
            }
            catch {
                print("Error with request: \(error)")
                return []
            }
        }
        return []
    }
}
