//
//  DateHelper.swift
//  marvelCharacters
//
//  Created by Pablo Sierra on 15/5/17.
//  Copyright © 2017 Pablo Sierra. All rights reserved.
//

import Foundation

extension Date {
    func marvelFormat () -> String {
        let dateformatter = DateFormatter()
        dateformatter.dateFormat = "yyyy-MM-dd"
        return dateformatter.string(from: Date())
    }
}
