//
//  CharacterCollectionViewCell.swift
//  marvelCharacters
//
//  Created by Pablo Sierra on 15/5/17.
//  Copyright © 2017 Pablo Sierra. All rights reserved.
//

import UIKit

class CharacterCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var bio: UILabel!
    @IBOutlet weak var thumbnail: UIImageView!
    var cellTag = 0
    
    override func prepareForReuse() {
        super.prepareForReuse()
        name.text = ""
        bio.text = ""
        thumbnail.image = nil
        thumbnail.alpha = 0
    }
    
    func loadCharacter(character: Character) {
        name.text = character.name
        bio.text = character.bio        
    }
}
