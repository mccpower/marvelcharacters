//
//  CryptoHelper.swift
//  marvelCharacters
//
//  Created by Pablo Sierra on 13/5/17.
//  Copyright © 2017 Pablo Sierra. All rights reserved.
//

import Foundation

struct CryptoHelper {
    static func MD5(string: String) -> String {
        let messageData = string.data(using:.utf8)!
        var digestData = Data(count: Int(CC_MD5_DIGEST_LENGTH))
        
        _ = digestData.withUnsafeMutableBytes {digestBytes in
            messageData.withUnsafeBytes {messageBytes in
                CC_MD5(messageBytes, CC_LONG(messageData.count), digestBytes)
            }
        }
        let digestHex = digestData.map { String(format: "%02hhx", $0) }.joined()
        return digestHex
    }
}
