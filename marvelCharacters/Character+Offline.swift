//
//  Character+Offline.swift
//  marvelCharacters
//
//  Created by Pablo Sierra on 14/5/17.
//  Copyright © 2017 Pablo Sierra. All rights reserved.
//

import Foundation
import CoreData

extension Character {
    var thumbnailURL: URL?  {
        get {
            if let path = self.thumbnailPath, let imageExtension = self.thumbnailExtension {
                return URL(string: path + "/standard_fantastic" + "." + imageExtension)
            }
            else {
                return nil
            }
        }
    }
    
    static func findCreate(fromdict dict: [String: Any]) -> Character {
        if let characterID = dict["id"] as? Int64, let character = self.findCharacter(id: characterID) {
            return self.update(character: character, dict: dict)
        }
        else {
            let characterObject = NSEntityDescription.insertNewObject(forEntityName: "Character", into: CoreDataManager.shared.managedObjectContext) as! Character
            return self.update(character: characterObject, dict: dict)
        }
    }
    
    static func update(character: Character, dict: [String: Any]) -> Character{
        //TODO: refactor to reuse characterID if possible //
        if let characterID = dict["id"] as? Int64 {
            character.id = characterID
        }
        character.bio = dict["description"] as? String
        character.name = dict["name"] as? String
        character.modified = dict["modified"] as? NSDate
        
        if let imageDict = dict["thumbnail"] as? [String: Any] {
            character.thumbnailExtension = imageDict["extension"] as? String
            if let path = imageDict["path"] as? String {
                character.thumbnailPath = path.replacingOccurrences(of: "http://", with: "https://")
            }
            
        }
        
        CoreDataManager.shared.saveContext(success: {
            print("Saved")
        }) { (errorDescription) in
            print("ERROR: \(errorDescription)")
        }
        return character
    }
    
    static func findCharacter(id: Int64) -> Character? {
        let request = NSFetchRequest<Character>(entityName: "Character")
        request.predicate = NSPredicate(format: "id == %d", id)
        
        do {
            let results = try CoreDataManager.shared.managedObjectContext.fetch(request)
            if let matchedCharacter = results.first {
                return matchedCharacter
            }
            else {
                return nil
            }
        } catch {
            return nil
        }
    }
    
    static func getCachedCharacters() -> [Character] {
        return CoreDataManager.shared.allRecords(type: "Character")
    }
    
}
