//
//  NetworkManager.swift
//  marvelCharacters
//
//  Created by Pablo Sierra on 13/5/17.
//  Copyright © 2017 Pablo Sierra. All rights reserved.
//

import Foundation

typealias NetworkSuccess = (_ result: Any?) -> Void
typealias NetworkFailure = (_ reason: FailureType, _ code: Int, _ errorDescription: String?) -> Void

enum FailureType {
    case authentication
    case timeout
    case noData
    case JSON
    case other
}

enum requestType: String {
    case GET = "GET"
    case POST = "POST"
    case PUT = "PUT"
}

class NetworkManager {
    static let shared = NetworkManager()
    
    func createRequest(urlString: String, type: requestType, parameters: Dictionary<String, Any>?) -> URLRequest? {
        if let requestURL = URL(string: urlString) {
            var request = URLRequest(url: requestURL)
            request.httpMethod = type.rawValue
            
            if let parameters = parameters {
                request.httpBody = try? JSONSerialization.data(withJSONObject: parameters, options: [])
            }
            print(urlString)
            return request
        }
        return nil
    }
    
    func performRequest(request: URLRequest, success: NetworkSuccess?, failure: NetworkFailure?) {
        URLSession.shared.dataTask(with: request) { (data, response, error) in
//            DispatchQueue.main.async {
                if let httpResponse = response as? HTTPURLResponse {
                    let code = httpResponse.statusCode
                    
                    switch code {
                    case 200:
                        if let data = data {
                            do {
                                let json = try JSONSerialization.jsonObject(with: data, options: []) 
                                success?(json)
                            } catch {
                                failure?(.JSON, code, error.localizedDescription)
                            }
                        }
                        else {
                            failure?(.noData, code, error?.localizedDescription)
                        }
                        
                    default:
                        if let data = data {
                            do {
                                let json = try JSONSerialization.jsonObject(with: data, options: [])
                                if let json = json as? NSDictionary, let status = json.object(forKey: "status") as? String {
                                   failure?(.other, code, status)
                                }
                            } catch {
                                failure?(.JSON, code, error.localizedDescription)
                            }
                        }
                        
                        failure?(.other, code, error?.localizedDescription)
                    }
                } else if let error = error as NSError? {
                    failure?(.timeout, error.code, error.localizedDescription)
                }
//            }
            }.resume()
    }
    
}
