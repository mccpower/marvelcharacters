# README #

It's required to obtain an API Key in the Marvel developer portal to be able to run the project. Set the public and private keys in the MarvelAPIConfiguration file.

### What is this repository for? ###

This is a simple app that pulls the Marvel characters using the official API.

### What is pending to do? ###

There are lot of improvements that can be made, some of them documented in the code with TODOs, either to improve the UI, or just to refactor and make code more reusable.

Unit tests!, initially I created one for testing the API, but there are many that can be written and I haven't written yet due the lack of time.


### Errors/known issues ###

It seems to be a crash that appears randomly in the first app open while saving in CD that I need to investigate further, as its related to save in CD from a background thread.

### Other ###

The image used for the launch screen has been set just for testing purposes and it shouldn't be used for any commercial use without the rights.