//
//  marvelCharactersTests.swift
//  marvelCharactersTests
//
//  Created by Pablo Sierra on 13/5/17.
//  Copyright © 2017 Pablo Sierra. All rights reserved.
//

import XCTest
@testable import marvelCharacters

class marvelCharactersTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testCharactersAPICall() {
        let charactersExpectation = expectation(description: "Get characters from Marvel API")
           MarvelAPIManager.shared.getCharacters(success: { (characters) in
            XCTAssert(characters.count > 0, "No characters found!")
            charactersExpectation.fulfill()
           }) { (errorDescription) in
            XCTFail(errorDescription)
        }
        
        waitForExpectations(timeout: 10) { (error) in
            if let error = error {
                XCTFail("Timeout pulling characters: \(error.localizedDescription)")
            }
        }
       
    }
    
}
